﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.PhantomJS;
using OpenQA.Selenium.Remote;
using System;
using System.Drawing.Imaging;
using System.IO;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Tracing;

namespace Voyager
{
    [TestFixture]
    public class NUnitTest1
    {
        private String browser = "Chrome";
        public IFileDetector FileDetector { get; set; }
        private IWebDriver _driver;

        [SetUp]
        public void Init()
        {
            DesiredCapabilities caps = new DesiredCapabilities();
            caps.SetCapability(CapabilityType.BrowserName, browser);
            caps.SetCapability("username", "paul1113");
            caps.SetCapability("accessKey", "iyA3C2hzjNdtZcuqUzRP");
            caps.SetCapability("name", TestContext.CurrentContext.Test.Name);
            _driver = new RemoteWebDriver(new Uri("http://ondemand.saucelabs.com:80/wd/hub"), caps, TimeSpan.FromSeconds(600));

            //_driver = new InternetExplorerDriver();

            IAllowsFileDetection fileDetectionDriver = _driver as IAllowsFileDetection;
            if (fileDetectionDriver != null)
            {
                fileDetectionDriver.FileDetector = new LocalFileDetector();
                FileDetector = new LocalFileDetector();
            }

        }

        [AfterScenario]
        public void AfterScenario()
        {
            TakeScreenshot(_driver);
        }

        public static void TakeScreenshot(IWebDriver driver)
        {
            try
            {
                string fileNameBase = string.Format("error_{0}_{1}_{2}",
                                                                    FeatureContext.Current.FeatureInfo.Title.ToIdentifier(),
                                                                    ScenarioContext.Current.ScenarioInfo.Title.ToIdentifier(),
                                                                    DateTime.Now.ToString("yyyyMMdd_HHmmss"));

                var artifactDirectory = Path.Combine(Directory.GetCurrentDirectory(), "testresults");
                if (!Directory.Exists(artifactDirectory))
                    Directory.CreateDirectory(artifactDirectory);

                Console.WriteLine(artifactDirectory);

                ITakesScreenshot takesScreenshot = driver as ITakesScreenshot;

                if (takesScreenshot != null)
                {
                    var screenshot = takesScreenshot.GetScreenshot();

                    string screenshotFilePath = Path.Combine(artifactDirectory, fileNameBase + "_screenshot.png");

                    screenshot.SaveAsFile(screenshotFilePath, ImageFormat.Png);

                    Console.WriteLine("Screenshot: {0}", new Uri(screenshotFilePath));
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error while taking screenshot: {0}", ex);
            }
        }


        [Test]
        public void TestMethod1()
        {   
            _driver.Navigate().GoToUrl("https://encodable.com/uploaddemo/");
            IWebElement query = _driver.FindElement(By.Id("uploadname1"));
            Assert.NotNull(query);
            var file = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "TestDocs/Book1.xlsx");

            _driver.FindElement(By.Id("formfield-email_address")).SendKeys("aaa@aaa.xom");
            _driver.FindElement(By.Id("formfield-first_name")).SendKeys("someone");

            query.SendKeys(file);

            TakeScreenshot(_driver);

            query.Submit();
        }

        [TearDown]
        public void CleanUp()
        {
            bool passed = true;
            try
            {
                // Logs the result to Sauce Labs
                ((IJavaScriptExecutor)_driver).ExecuteScript("sauce:job-result=" + (passed ? "passed" : "failed"));
            }
            finally
            {
                // Terminates the remote webdriver session
                _driver.Quit();
            }
        }
    }    
}