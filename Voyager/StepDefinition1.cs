﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace Voyager
{
    [Binding]
    public sealed class StepDefinition1
    {
        IWebDriver _driver;

        [Given(@"I am on google")]
        public void GivenIAmOnGoogle()
        {
            DesiredCapabilities caps = new DesiredCapabilities();
            caps.SetCapability(CapabilityType.BrowserName, "Chrome");
            caps.SetCapability("username", "paultaylor");
            caps.SetCapability("accessKey", "31af0d17-4c56-44e6-ae3b-57fe389c1395");
            caps.SetCapability("name", TestContext.CurrentContext.Test.Name);
            _driver = new RemoteWebDriver(new Uri("http://ondemand.saucelabs.com:80/wd/hub"), caps, TimeSpan.FromSeconds(600));
            ScenarioContext.Current.Set(_driver);

            _driver.Navigate().GoToUrl("http://www.google.com");
        }

        [Then(@"I should be on google")]
        public void ThenIShouldBeOnGoogle()
        {
            Assert.True(_driver.PageSource.Contains("google"));
        }
    }
}
